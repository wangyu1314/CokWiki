#CokWiki
>CokWiki是运行于Node平台的类Wiki程序，使用Markdown语法代替传统Wiki语法进行词条编辑。基于[NodeMVC](https://git.oschina.net/cokapp/NodeMVC)实现。

##特色
1. 微内核，一个主控制器+4个外围插件
2. 使用文本文件存储词条，无需数据库支持
3. 支持多主题
4. 支持Docker`docker pull cokapp/cokwiki`

##在Docker中使用
容器通过内部80端口提供服务，请绑定80端口到外部任意端口

1. `docker pull cokapp/cokwiki`
2. `docker docker run -d -p 80:80`

##其他
参见[DEMO](http://cokwiki.coding.io "host on coding.io")。

##版本历史
1. `2014-09-12` 初始版本
2. `2015-08-03` 支持Docker